#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet::~Planet()
{
	delete mNode;
}

Planet* Planet::createPlanet(SceneManager &sceneManager, float size, ColourValue colour, Vector3 location)
{
	ManualObject* object = sceneManager.createManualObject();
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Colour
	object->colour(colour);

	//Front
	object->position(-(size/2), size/2, size/2);
	object->position(-(size/2), -(size/2), size/2);
	object->position(size/2, -(size/2), size/2);

	object->position(size/2, size/2, size/2);
	object->position(-(size/2), size/2, size/2);
	object->position(size/2, -(size/2), size/2);

	//Back
	object->position(-(size/2), size/2, -(size/2));
	object->position(size/2, -(size/2), -(size/2));
	object->position(-(size/2), -(size/2), -(size/2));

	object->position(size/2, size/2, -(size/2));
	object->position(size/2, -(size/2), -(size/2));
	object->position(-(size/2), size/2, -(size/2));


	//Left
	object->position(-(size/2), -(size/2), -(size/2));
	object->position(-(size/2), -(size/2), size/2);
	object->position(-(size/2), size/2, -(size/2));

	object->position(-(size/2), -(size/2), size/2);
	object->position(-(size/2), size/2, size/2);
	object->position(-(size/2), size/2, -(size/2));

	//Right
	object->position(size/2, -(size/2), size/2);
	object->position(size/2, -(size/2), -(size/2));
	object->position(size/2, size/2, -(size/2));

	object->position(size/2, -(size/2), size/2);
	object->position(size/2, size/2, -(size/2));
	object->position(size/2, size/2, size/2);

	//Top
	object->position(-(size/2), size/2, size/2);
	object->position(size/2, size/2, size/2);
	object->position(size/2, size/2, -(size/2));

	object->position(size/2, size/2, -(size/2));
	object->position(-(size/2), size/2, -(size/2));
	object->position(-(size/2), size/2, size/2);

	//Bottom
	object->position(-(size/2), -(size/2), size/2);
	object->position(size/2, -(size/2), -(size/2));
	object->position(size/2, -(size/2), size/2);

	object->position(size/2, -(size/2), -(size/2));
	object->position(-(size/2), -(size/2), size/2);
	object->position(-(size/2), -(size/2), -(size/2));

	object->end();

	SceneNode* newNode;
	newNode = sceneManager.getRootSceneNode()->createChildSceneNode(location);
	newNode->attachObject(object);
	Planet* planet = new Planet(newNode);

	return planet;
}

SceneNode& Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
	float pX = mParent->getNode().getPosition().x;
	float pZ = mParent->getNode().getPosition().z;

	mNode->setPosition(mNode->getPosition().x + pX, 0, mNode->getPosition().z + pZ);
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}

void Planet::update(const FrameEvent& evt)
{
	Degree rotation = Degree(mLocalRotationSpeed);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation) * evt.timeSinceLastFrame);

	if (mParent == NULL)
		return;

	float pX = mParent->getNode().getPosition().x;
	float pZ = mParent->getNode().getPosition().z;

	const float oldX = mNode->getPosition().x - pX;
	const float oldZ = mNode->getPosition().z - pZ;
	const float oldY = mNode->getPosition().y;

	float newX = oldX * cos(mRevolutionSpeed * evt.timeSinceLastFrame) + oldZ * sin(mRevolutionSpeed * evt.timeSinceLastFrame);
	float newZ = oldX * -sin(mRevolutionSpeed * evt.timeSinceLastFrame) + oldZ * cos(mRevolutionSpeed * evt.timeSinceLastFrame);

	mNode->setPosition(newX + pX, oldY, newZ + pZ);
}
