#include <iostream>
#include <time.h>
#include <string>
#include "Player.h"
#include "UIHandler.h"
#include "GachaMachine.h"

using namespace std;

string result(Player* player)
{
	if (player->getCrystals() <= 0 || player->getHp() <= 0) return "LOSE"; 
	if (player->getRarityPts() >= 100) return "WIN";
}

int main()
{
	srand(time(NULL));
	Player* player = new Player(100, 100);
	UIHandler* ui = new UIHandler();
	GachaMachine* gacha = new GachaMachine(player);

	while (true)
	{
		ui->printUI(player);

		Item* item = gacha->roll(player);
		gacha->returnRoll(player, item);
		
		system("pause");
		system("CLS");

		if (player->getCrystals() <= 0 || player->getHp() <= 0 || player->getRarityPts() >= 100) break;
	}

	ui->printSummary(player);
	system("CLS");

	string results = result(player);
	ui->collateItems(player, gacha, results);

	return 0;
}