#pragma once
#include <vector>
#include "Item.h"
#include <iostream>

using namespace std; 

class Item;
class Player
{
public:
	Player(int pStartCrystals, int pHp);
	~Player();

	int getHp();
	int getRarityPts();
	int getCrystals();
	int getPulls();
	void deductCrystals(int value);
	void subtractHp(int value);
	void addInventory(Item* item);
	vector<Item*> getInventory();
	void addPulls();
	void addRarityPts(int value);

private:
	int hp;
	int rarityPts;
	int crystals;
	int pulls;
	vector<Item*> inventory;
};

