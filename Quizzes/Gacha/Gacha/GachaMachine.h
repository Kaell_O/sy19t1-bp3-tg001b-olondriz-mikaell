#pragma once
#include <vector>
#include <iostream>
#include <string>
#include "RareItem.h"
#include "Bomb.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "Item.h"
#include "Player.h"

class GachaMachine
{
public:
	GachaMachine(Player* player);
	Item* roll(Player* player);
	void returnRoll(Player* player, Item* item);
	vector<Item*> getStock();
	
private:
	vector<Item*> stock;
};

