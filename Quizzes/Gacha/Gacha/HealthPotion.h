#pragma once
#include "Item.h"

class Player;
class HealthPotion : public Item
{
public:
	HealthPotion(string pName);
	~HealthPotion();
	void doEffect(Player* player);

private:
	int heal;
};

