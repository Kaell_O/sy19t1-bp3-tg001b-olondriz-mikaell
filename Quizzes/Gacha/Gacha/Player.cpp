#include "Player.h"
#include <algorithm>

Player::Player(int pStartCrystals, int pHp)
{
	crystals = pStartCrystals;
	hp = pHp;
}


Player::~Player()
{
}

int Player::getHp()
{
	return hp;
}

int Player::getRarityPts()
{
	return rarityPts;
}

int Player::getPulls()
{
	return pulls;
}

void Player::addPulls()
{
	pulls += 1;
}

int Player::getCrystals()
{
	return crystals;
}

void Player::deductCrystals(int value)
{
	crystals -= value;
}

vector<Item*> Player::getInventory()
{
	return inventory;
}

void Player::addInventory(Item* item)
{
	inventory.push_back(item);
	item->doEffect(this);
}

void Player::subtractHp(int value)
{
	hp -= value;
}

void Player::addRarityPts(int value)
{
	rarityPts += value;
}