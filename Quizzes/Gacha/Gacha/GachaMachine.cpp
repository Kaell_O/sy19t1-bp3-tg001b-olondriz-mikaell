#include "GachaMachine.h"

GachaMachine::GachaMachine(Player* player)
{
	stock.push_back(new RareItem(player, 1, "R Item"));
	stock.push_back(new Bomb("Bomb"));
	stock.push_back(new Crystal("15 Crystals Pack"));
	stock.push_back(new HealthPotion("Health Potion"));
	stock.push_back(new RareItem(player, 10, "SR Item"));
	stock.push_back(new RareItem(player, 50, "SSR Item"));
}

Item* GachaMachine::roll(Player* player)
{
	int randomNumber = rand() % 100 + 1;

	if (randomNumber <= 40) return stock[0];
	else if (randomNumber <= 60) return stock[1];
	else if (randomNumber <= 75) return stock[2];
	else if (randomNumber <= 90) return stock[3];
	else if (randomNumber <= 99) return stock[4];
	else if (randomNumber == 100) return stock[5];
}

void GachaMachine::returnRoll(Player* player, Item* item)
{
	item->deductCrystals(player);
	player->addPulls();
	player->addInventory(item);
	item->printItemGet();
}

vector<Item*> GachaMachine::getStock()
{
	return stock;
}