#pragma once
#include <iostream>
#include "RareItem.h"
#include "Bomb.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "Item.h"
#include "Player.h"
#include "GachaMachine.h"

using namespace std;

class UIHandler
{
public:
	void printSummary(Player* player);
	void printUI(Player* player);
	void collateItems(Player* player, GachaMachine* gacha, string result);
};

