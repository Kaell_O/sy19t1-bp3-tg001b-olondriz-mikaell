#include "Crystal.h"
#include "Player.h"

Crystal::Crystal(string pName) : Item(pName)
{
	crystals = 15;
}


Crystal::~Crystal()
{
}

void Crystal::doEffect(Player* player)
{
	player->deductCrystals(-crystals);
	cout << "You receive " << crystals << " Crystals!" << endl;
}
