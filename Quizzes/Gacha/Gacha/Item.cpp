#include "Item.h"
#include "Player.h"


Item::Item(string pName)
{
	name = pName;
	itemCount = 0;
}

Item::~Item()
{
}

string Item::getName()
{
	return name;
}

void Item::doEffect(Player* player)
{
}

void Item::deductCrystals(Player* player)
{
	player->deductCrystals(5);
}

void Item::addItemCount()
{
	itemCount += 1;
}

void Item::printItemGet()
{
	cout << "You got 1 <" << name << ">" << endl;
}

int Item::getItemCount()
{
	return itemCount;
}