#include "UIHandler.h"

void UIHandler::printSummary(Player* player)
{
	cout << "========== SUMMARY OF ITEMS ==========" << endl;
	vector<Item*> inventory = player->getInventory();
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i]->getName() << endl;
	}
	system("pause");
}

void UIHandler::printUI(Player* player)
{
	cout << "Health: " << player->getHp() << endl;
	cout << "Crystals: " << player->getCrystals() << endl;
	cout << "Pulls: " << player->getPulls() << endl;
	cout << "Rarity Points: " << player->getRarityPts() << endl;
	cout << "==============================" << endl;
}

void UIHandler::collateItems(Player* player, GachaMachine* gacha, string result)
{
	cout << "============ RESULTS ============" << endl;
	cout << "Remaining HP: " << player->getHp() << endl;
	cout << "Remaining Crystals: " << player->getCrystals() << endl;
	cout << "Total Pulls: " << player->getPulls() << endl;
	cout << "Rarity Points: " << player->getRarityPts() << endl << endl;
	vector<Item*> stock = gacha->getStock();
	vector<Item*> inventory = player->getInventory();

	//Count
	for (int x = 0; x < stock.size(); x++)
	{
		for (int i = 0; i < inventory.size(); i++)
		{
			if (inventory[i]->getName() == stock[x]->getName()) stock[x]->addItemCount();
		}
	}

	cout << "========== ITEMS AQUIRED ==========" << endl;

	for (int i = 0; i < stock.size(); i++)
	{
		if (stock[i]->getItemCount() != 0)
		cout << stock[i]->getName() << " x " << stock[i]->getItemCount() << endl;
	}

	cout << endl;
	cout << "RESULT: " << result << endl;
	system("pause");
}