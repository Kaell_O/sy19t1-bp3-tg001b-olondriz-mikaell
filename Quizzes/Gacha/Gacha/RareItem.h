#pragma once
#include "Item.h"

class Player;
class RareItem : public Item
{
public:
	RareItem(Player* player, int pRarityPoints, string pName);
	//using Item::Item; is this better than the first option?
	~RareItem();

	void doEffect(Player* player);

private:
	int rarityPoints;
};


