#pragma once
#include "Item.h"

class Player;
class Bomb : public Item
{
public:
	Bomb(string pName);
	~Bomb();

	void doEffect(Player* player);

private:
	int damage;
};

