#pragma once
#include "Item.h"

class Player;
class Crystal : public Item
{
public:
	Crystal(string pName);
	~Crystal();

	void doEffect(Player* player);

private:
	int crystals;
};

