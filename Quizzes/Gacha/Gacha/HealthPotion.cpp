#include "HealthPotion.h"
#include "Player.h"


HealthPotion::HealthPotion(string pName) : Item(pName)
{
	heal = 30;
}


HealthPotion::~HealthPotion()
{
}

void HealthPotion::doEffect(Player* player)
{
	player->subtractHp(-heal);
	cout << "Potion heals for " << heal << " HP!" << endl;
}
