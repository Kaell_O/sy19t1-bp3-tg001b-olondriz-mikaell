#pragma once
#include <string>

using namespace std;

class Player;
class Item
{
public:
	Item(string pName);
	~Item();

	virtual void doEffect(Player* player);
	void deductCrystals(Player* player);
	void printItemGet();
	int getItemCount();
	void addItemCount();

	string getName();

protected:
	string name;
	int cost;
	int itemCount;
};

