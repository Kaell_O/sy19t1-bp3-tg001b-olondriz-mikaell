#include "RareItem.h"
#include "Player.h"


RareItem::RareItem(Player* player, int pRarityPoints, string pName) : Item(pName)
{
	rarityPoints = pRarityPoints;
}

void RareItem::doEffect(Player* player)
{
	player->addRarityPts(rarityPoints);
}

RareItem::~RareItem()
{
}
