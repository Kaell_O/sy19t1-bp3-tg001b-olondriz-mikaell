#include "Bomb.h"
#include "Player.h"


Bomb::Bomb(string pName) : Item(pName)
{
	damage = 25;
}

Bomb::~Bomb()
{
}

void Bomb::doEffect(Player* player)
{
	player->subtractHp(damage);
	cout << "The bomb explodes and deals " << damage << "!" << endl;
}