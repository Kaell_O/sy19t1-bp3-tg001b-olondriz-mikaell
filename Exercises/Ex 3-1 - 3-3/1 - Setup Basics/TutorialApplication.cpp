/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "OgreManualObject.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	ManualObject *object = mSceneMgr->createManualObject();

	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Front
	object->position(0, 10, 0);
	object->colour(0.5, 0, 0);
	object->position(0, 0, 0);
	object->position(10, 0, 0);

	object->position(10, 10, 0);
	object->position(0, 10, 0);
	object->position(10, 0, 0);

	//Back
	object->position(0, 10, -10);
	object->position(10, 0, -10);
	object->position(0, 0, -10);

	object->position(10, 10, -10);
	object->position(10, 0, -10);
	object->position(0, 10, -10);

	//Left
	object->position(0, 0, -10);
	object->position(0, 0, 0);
	object->position(0, 10, -10);

	object->position(0, 0, 0);
	object->position(0, 10, 0);
	object->position(0, 10, -10);

	//Right
	object->position(10, 0, 0);
	object->position(10, 0, -10);
	object->position(10, 10, -10);

	object->position(10, 0, 0);
	object->position(10, 10, -10);
	object->position(10, 10, 0);

	//Top
	object->position(0, 10, 0);
	object->position(10, 10, 0);
	object->position(10, 10, -10);

	object->position(10, 10, -10);
	object->position(0, 10, -10);
	object->position(0, 10, 0);


	//Bottom
	object->position(0, 0, 0);
	object->position(10, 0, -10);
	object->position(10, 0, 0);
	

	object->position(10, 0, -10);
	object->position(0, 0, 0);
	object->position(0, 0, -10);
	

	object->end();

	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);
}

void TutorialApplication::movement(const FrameEvent& evt)
{
	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		cubeNode->translate(0, 0, speed * evt.timeSinceLastFrame);
		isMoving = true;
	}
		
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		cubeNode->translate(0, 0, -speed * evt.timeSinceLastFrame);
		isMoving = true;
	}
	
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		cubeNode->translate(-speed * evt.timeSinceLastFrame, 0, 0);
		isMoving = true;
	}
		
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		cubeNode->translate(speed * evt.timeSinceLastFrame, 0, 0);
		isMoving = true;
	}
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	isMoving = false;

	movement(evt);
	if (isMoving)
	{
		speed += accRate * evt.timeSinceLastFrame;
		if (speed > maxSpeed)
			speed = maxSpeed;
	}
	else
	{
		speed = baseSpeed;
	}
		
	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
