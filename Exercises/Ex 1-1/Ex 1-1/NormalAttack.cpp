#include "NormalAttack.h"
#include "Character.h"

NormalAttack::NormalAttack()
{
	mpCost = 0;
}


NormalAttack::~NormalAttack()
{
}

void NormalAttack::useSkill()
{
	cout << character->getName() << " does a Normal Attack!" << endl;
}
