#pragma once
#include <string>
#include <iostream>

using namespace std;

class Character;
class Skill
{
public:
	Skill();
	~Skill();

	virtual void useSkill();


protected:
	int damage;
	int mpCost;
	string name;
	Character* character; //needs to define the constructor 
};

