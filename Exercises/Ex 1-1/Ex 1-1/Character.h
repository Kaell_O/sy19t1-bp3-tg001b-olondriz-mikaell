#pragma once
#include <string>
#include <time.h>
#include <iostream>
#include <vector>
#include "Weapon.h"
#include "Skill.h"
#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Vengeance.h"
#include "Syphon.h"
#include "DopalBlade.h"

using namespace std;

class Character
{
public:
	Character(string chosenName, int health, int mana, Weapon* currentWeapon);
	~Character();

	void attack(Character* player, Character* enemy);
	void useAndPrintSkill(int skillChosen, Character* player, Character* enemy, bool &cloned);
	void printUI(Character* player);

	int getHp();
	string getName();
	int getDamage();
	vector<Skill*> getSkills();

private:
	string name;
	int hp;
	int mp;
	Weapon* weapon;
	vector<Skill*> skills;
};

