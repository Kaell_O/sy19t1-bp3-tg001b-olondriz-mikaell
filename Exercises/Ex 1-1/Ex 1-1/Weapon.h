#pragma once
#include <string>
using namespace std;
class Weapon
{
public:
	Weapon(string pName, int pDamage);
	~Weapon();
	int getDamage();

private:
	string name;
	int damage;
};

