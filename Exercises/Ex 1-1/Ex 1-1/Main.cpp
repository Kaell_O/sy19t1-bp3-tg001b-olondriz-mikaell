#include <iostream>
#include <string>
#include <time.h>
#include "Weapon.h"
#include "Character.h"

using namespace std;

void ending(Character* player, Character* enemy)
{
	if (player->getHp() <= 0 && enemy->getHp() <= 0) cout << "TIE!" << endl;
	else if (player->getHp() <= 0) cout << enemy->getName() << " WINS!" << endl;
	else if (enemy->getHp() <= 0) cout << player->getName() << " WINS!" << endl;
}

int main()
{
	srand(time(NULL));
	Weapon* zweihander = new Weapon("Zweihander", 10);
	Weapon* darkSword = new Weapon("Dark Sword", 10);
	Character* player = new Character("Siegward", 100, 10, zweihander);
	Character* enemy = new Character("Dark Wraith", 100, 10, darkSword);

	while (true)
	{	
		player->printUI(player);
		player->printUI(enemy);
		cout << endl;

		cout << "---PLAYER'S TURN---" << endl << endl;
		player->attack(player, enemy);
		system("pause");
		cout << endl;

		cout << "---ENEMY'S TURN---" << endl << endl;
		enemy->attack(enemy, player);
		system("pause");
		system("CLS");

		if (player->getHp() <= 0 || enemy->getHp() <= 0) break;
	}
	ending(player, enemy);

	system("pause");
	return 0;
}