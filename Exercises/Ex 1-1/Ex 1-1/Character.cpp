#include "Character.h"

Character::Character(string pName, int pHP, int pMana, Weapon* pWeapon)
{
	name = pName;
	hp = pHP;
	mp = pMana;
	weapon = pWeapon;

	skills.push_back(new NormalAttack());
	skills.push_back(new CriticalAttack());
	skills.push_back(new Syphon());
	skills.push_back(new Vengeance());
	skills.push_back(new DopalBlade());
}

Character::~Character()
{

}

string Character::getName()
{
	return name;
}

int Character::getHp()
{
	return hp;
}

int Character::getDamage()
{
	return weapon->getDamage();
}

vector<Skill*> Character::getSkills()
{
	return skills;
}

void Character::printUI(Character* player)
{
	cout << player->name << " HP: " << player->hp << endl;
}

void Character::attack(Character* player, Character* enemy)
{
	int skillChosen = rand() % 5 + 1;
	bool cloned;

	useAndPrintSkill(skillChosen, player, enemy, cloned);

	while (cloned == true)
	{
		system("pause");
		Character *clone = new Character(player->name + "'s Clone", player->getHp(), 10, player->weapon);
		skillChosen = rand() % 5 + 1;

		cout << endl;
		useAndPrintSkill(skillChosen, clone, enemy, cloned);

		delete clone;
	}
}

void Character::useAndPrintSkill(int skillChosen, Character* player, Character* enemy, bool &cloned)
{
	int damage = player->weapon->getDamage();
	int random = rand() % 5 + 1;

	switch (skillChosen)
	{
	case 1:
		cout << player->name << " does a Normal Attack!" << endl;
		break;
	case 2:
		cout << player->name << " does a Critical Strike!" << endl;
		if (random == 5) {
			cout << "Critical Strike Successful!" << endl;
			damage *= 2;
		}
		else cout << "Critcal Strike Failed!" << endl;

		break;
	case 3:
		cout << player->name << " uses Syphon!" << endl;
		cout << player->name << " saps " << .25 * damage << " HP from " << enemy->name << endl;
		cout << player->name << "'s HP: " << player->hp << " -> ";
		player->hp += .25 * damage;
		cout << player->hp << endl;

		break;
	case 4:
	{
		float damageFromHp = .25 * player->getHp();
		cout << player->name << " uses Vengeance!" << endl;
		cout << player->name << "'s rage fueled attack recoils for " << damageFromHp << " HP " << endl;
		cout << player->name << "'s HP: " << player->hp << " -> ";

		player->hp -= floor(damageFromHp);
		cout << player->hp << endl;

		damage += damageFromHp * 2;
	}
		break;
	case 5:
		cout << player->name << " uses Dopal Blade!" << endl;
		cout << player->name << " splits into two and creates a clone!" << endl;
		cout << player->name << " hits " << enemy->name << " for " << damage << endl;
		enemy->hp -= damage;
		cloned = true;
		return;
	}

	cout << endl;
	cout << player->name << " hits " << enemy->name << " for " << damage << endl;
	enemy->hp -= damage;
	cloned = false;
}