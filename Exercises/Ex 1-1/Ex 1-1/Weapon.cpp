#include "Weapon.h"

Weapon::Weapon(string pName, int pDamage)
{
	name = pName;
	damage = pDamage;
}

Weapon::~Weapon()
{

}

int Weapon::getDamage()
{
	return damage;
}