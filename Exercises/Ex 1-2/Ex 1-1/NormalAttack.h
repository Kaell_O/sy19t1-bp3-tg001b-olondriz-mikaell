#pragma once
#include "Skill.h"

class Character;
class NormalAttack : public Skill
{
public:
	NormalAttack();
	~NormalAttack();

	void useSkill(Character* caster, Character* target);
};

