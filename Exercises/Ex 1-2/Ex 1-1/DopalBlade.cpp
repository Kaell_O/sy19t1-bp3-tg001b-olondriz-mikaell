#include "DopalBlade.h"
#include "Character.h"
#include "Weapon.h"

DopalBlade::DopalBlade()
{
}


DopalBlade::~DopalBlade()
{
}

void DopalBlade::useSkill(Character* caster, Character* target)
{
	//Caster's Turn
	damage = caster->getDamage();
	cout << caster->getName() << " uses Dopal Blade!" << endl;
	cout << caster->getName() << " splits into two and creates a clone!" << endl;
	cout << caster->getName() << " hits " << target->getName() << " for " << damage << endl;
	caster->applyDamage(target, damage);

	//Clone's Turn
	system("pause");
	Weapon* cloneWeapon = new Weapon("Clone's Weapon", caster->getWeapon()->getDamage());
	Character *clone = new Character(caster->getName() + "'s Clone", caster->getHp(), 10, cloneWeapon);
	int skillChosen = rand() % 5;
	cout << endl;

	vector<Skill*> cloneSkills = clone->getSkills();
	cloneSkills[skillChosen]->useSkill(clone, target); 
	delete clone;
}