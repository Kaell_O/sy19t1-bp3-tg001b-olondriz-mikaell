#pragma once
#include <string>
#include <iostream>

using namespace std;

class Character;
class Skill
{
public:
	Skill();
	~Skill();
	virtual void useSkill(Character* caster, Character* target);

protected:
	int damage;
	int mpCost;
	string spellName;
	string casterName;
	string targetName;
	int casterHp;
	int targetHp;
};
