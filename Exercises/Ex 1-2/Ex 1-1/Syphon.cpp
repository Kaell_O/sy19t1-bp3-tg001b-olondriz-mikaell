#include "Syphon.h"
#include "Character.h"


Syphon::Syphon()
{
}


Syphon::~Syphon()
{
}


void Syphon::useSkill(Character* caster, Character* target)
{
	damage = caster->getDamage();
	cout << caster->getName() << " uses Syphon!" << endl;
	cout << caster->getName() << " saps " << .25 * damage << " HP from " << target->getName() << endl;
	cout << caster->getName() << "'s HP: " << caster->getHp() << " -> ";
	caster->applyDamage(caster, -(.25 * damage)); //heal caster
	cout << caster->getHp() << endl;

	caster->applyDamage(target, damage);
	cout << caster->getName() << " hits " << target->getName() << " for " << damage << " damage!" << endl;
}