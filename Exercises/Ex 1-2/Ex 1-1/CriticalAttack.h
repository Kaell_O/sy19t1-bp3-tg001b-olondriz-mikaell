#pragma once
#include "Skill.h"

class Character;
class CriticalAttack : public Skill
{
public:
	CriticalAttack();
	~CriticalAttack();
	void useSkill(Character* caster, Character* target);
};

