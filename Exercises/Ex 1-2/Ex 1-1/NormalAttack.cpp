#include "NormalAttack.h"
#include "Character.h"

NormalAttack::NormalAttack()
{
	mpCost = 0;
}


NormalAttack::~NormalAttack()
{
}

void NormalAttack::useSkill(Character* caster, Character* target)
{
	damage = caster->getDamage();
	cout << caster->getName() << " does a Normal Attack!" << endl;
	cout << caster->getName() << " hits " << target->getName() << " for " << damage << " damage!" << endl;
	caster->applyDamage(target, damage);
}
