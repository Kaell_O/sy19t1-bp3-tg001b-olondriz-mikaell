#pragma once
#include <string>
#include <time.h>
#include <iostream>
#include <vector>
#include "Weapon.h"
#include "Skill.h"
#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Vengeance.h"
#include "Syphon.h"
#include "DopalBlade.h"

using namespace std;

class Character
{
public:
	Character(string chosenName, int health, int mana, Weapon* currentWeapon);
	~Character();

	void attack(Character* player, Character* enemy);
	void printUI(Character* player);
	void applyDamage(Character* caster, int damage);

	int getHp();
	string getName();
	int getDamage();
	Weapon* getWeapon();
	vector<Skill*> getSkills();

private:
	string name;
	int hp;
	int mp;
	Weapon* weapon;
	vector<Skill*> skills;
};

