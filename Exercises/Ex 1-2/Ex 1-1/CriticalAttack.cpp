#include "CriticalAttack.h"
#include "Character.h"


CriticalAttack::CriticalAttack()
{
}


CriticalAttack::~CriticalAttack()
{
}

void CriticalAttack::useSkill(Character* caster, Character* target)
{
	int random = rand() % 5 + 1;
	damage = caster->getDamage();
	cout << caster->getName() << " does a Critical Strike!" << endl;
	if (random == 5) {
		cout << "Critical Strike Successful!" << endl;
		damage *= 2;
	}
	else cout << "Critcal Strike Failed!" << endl;

	caster->applyDamage(target, damage);
	cout << caster->getName() << " hits " << target->getName() << " for " << damage << " damage!" << endl;
}