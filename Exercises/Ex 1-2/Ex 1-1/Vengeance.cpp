#include "Vengeance.h"
#include "Character.h"


Vengeance::Vengeance()
{
}


Vengeance::~Vengeance()
{
}

void Vengeance::useSkill(Character* caster, Character* target)
{
	damage = caster->getDamage();
	float damageFromHp = .25 * caster->getHp();
	cout << caster->getName() << " uses Vengeance!" << endl;
	cout << caster->getName() << "'s rage fueled attack recoils for " << damageFromHp << " HP " << endl;
	cout << caster->getName() << "'s HP: " << caster->getHp() << " -> ";

	caster->applyDamage(caster, floor(damageFromHp));

	cout << caster->getHp() << endl;

	damage += damageFromHp * 2;
	caster->applyDamage(target, damage);

	cout << caster->getName() << " hits " << target->getName() << " for " << damage << " damage!" << endl;
}
