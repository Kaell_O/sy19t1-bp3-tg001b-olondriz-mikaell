#pragma once
#include "Skill.h"

class Character;
class Vengeance : public Skill
{
public:
	Vengeance();
	~Vengeance();
	void useSkill(Character* caster, Character* target);
};

