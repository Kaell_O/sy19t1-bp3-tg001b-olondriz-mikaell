#pragma once
#include "Skill.h"

class Character;
class Syphon : public Skill
{
public:
	Syphon();
	~Syphon();

	void useSkill(Character* caster, Character* target);
};

