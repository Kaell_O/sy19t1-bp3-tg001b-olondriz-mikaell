#pragma once
#include "Skill.h"

class Character;
class DopalBlade : public Skill
{
public:
	DopalBlade();
	~DopalBlade();
	void useSkill(Character* caster, Character* target);
};

