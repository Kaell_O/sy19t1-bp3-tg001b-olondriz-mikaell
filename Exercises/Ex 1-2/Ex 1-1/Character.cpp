#include "Character.h"

Character::Character(string pName, int pHP, int pMana, Weapon* pWeapon)
{
	name = pName;
	hp = pHP;
	mp = pMana;
	weapon = pWeapon;

	skills.push_back(new NormalAttack());
	skills.push_back(new CriticalAttack());
	skills.push_back(new Syphon());
	skills.push_back(new Vengeance());
	skills.push_back(new DopalBlade());
}

Character::~Character()
{
	delete weapon;
}

string Character::getName()
{
	return name;
}

int Character::getHp()
{
	return hp;
}

int Character::getDamage()
{
	return weapon->getDamage();
}

vector<Skill*> Character::getSkills()
{
	return skills;
}

Weapon* Character::getWeapon()
{
	return weapon;
}

void Character::printUI(Character* player)
{
	cout << player->name << " HP: " << player->hp << endl;
}

void Character::applyDamage(Character* target, int damage)
{
	target->hp -= damage;
}

void Character::attack(Character* player, Character* enemy)
{
	int skillChosen = rand() % 5;
	skills[skillChosen]->useSkill(player, enemy);
}