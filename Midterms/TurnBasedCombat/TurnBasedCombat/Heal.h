#pragma once
#include "Skill.h"
class Heal : public Skill
{
public:
	Heal(string name, string job, string team, int cost);
	void printSkill(Unit* caster, Unit* target);
	deque<Unit*> autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	deque<Unit*> manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	void setHeal(Unit* caster, Unit* target);
	void castSkill(Unit* caster, deque<Unit*> target);

private:
	int healValue;
};

