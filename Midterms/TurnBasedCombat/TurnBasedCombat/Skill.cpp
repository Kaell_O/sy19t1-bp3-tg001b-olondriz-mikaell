#include "Skill.h"

Skill::Skill(string pname, string pjob, string pteam) 
{
	cost = 0;
	name = pname;
	job = pjob;
	team = pteam;
}

void Skill::printSkill(Unit* caster, Unit* target)
{
	cout << "Default" << endl;
}

deque<Unit*> Skill::autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	return deque<Unit*>();
}

deque<Unit*> Skill::manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();
	int input;
	cout << "> Choose target <" << endl;
	for (size_t i = 0; i < enemyTeam.size(); i++)
	{
		cout << "[" << i + 1 << "] " << enemyTeam[i]->getStats().name << " HP [" <<
			enemyTeam[i]->getStats().hp << "]" << endl;
	}
	cin >> input;
	targets.push_back(enemyTeam[input]);
	return targets;
}

void Skill::castSkill(Unit* caster, deque<Unit*> target)
{
	cout << caster->getStats().name << " used " << name << "!" << endl << endl;

	for (size_t i = 0; i < target.size(); i++)
	{
		setDamage(caster, target[i]);
		printSkill(caster, target[i]);
		target[i]->applyDamage(damage);
	}
}

string Skill::getName()
{
	return name;
}

string Skill::getJob()
{
	return job;
}

string Skill::getTeam()
{
	return team;
}

int Skill::getCost()
{
	return cost;
}

int Skill::getDamage()
{
	return damage;
}

void Skill::setDamage(Unit* caster, Unit* target)
{
	baseDamage = ceil(caster->getStats().pow + (rand() % caster->getStats().pow * 0.2));
	damage = caster->getDamage(target, baseDamage);
}
