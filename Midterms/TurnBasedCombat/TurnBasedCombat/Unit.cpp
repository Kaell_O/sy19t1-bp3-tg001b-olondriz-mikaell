#include "Unit.h"

Unit::Unit(CharacterDef definition)
{
	stats.job = definition.job;
	stats.team = definition.team;
	stats.name = definition.name;
	stats.hp = definition.hp;
	stats.mp = definition.mp;
	stats.pow = definition.pow;
	stats.vit = definition.vit;
	stats.agi = definition.agi;
	stats.dex = definition.dex;
	stats.maxHp = stats.hp;
	stats.maxMp = stats.mp;

	if (stats.job == "warrior")
		strongAgainst = "assassin";
	if (stats.job == "mage")
		strongAgainst = "warrior";
	if (stats.job == "assassin")
		strongAgainst = "mage";
}

Unit::~Unit()
{
	for (size_t i = 0; i < skills.size(); i++)
	{
		delete skills[i];
	}
	skills.clear();
}

bool Unit::chooseTarget()
{
	return false;
}

CharacterDef Unit::getStats()
{
	return stats;
}

int Unit::getDamage(Unit* target, int baseDamage)
{
	float bonusDamage = 1;
	if (target->getStats().job == strongAgainst)
		bonusDamage = 1.5;

	int damage = (baseDamage - target->getStats().vit) * bonusDamage;
	if (damage <= 0)
		damage = 1;
	
	return damage;
}

Skill* Unit::getSkill()
{
	return skill;
}

deque<Unit*> Unit::getTarget()
{
	return target;
}


void Unit::applyDamage(int damage)
{
	stats.hp -= damage;
}

void Unit::addSkill(Skill* skill)
{
	skills.push_back(skill);
}

void Unit::heal(int value)
{
	stats.hp += value;
	if (stats.hp > stats.maxHp)
		stats.hp = stats.maxHp;
}

void Unit::printStats()
{
	cout << "=================<" << stats.name << ">=================" << endl;
	cout << "HP: " << stats.hp << "/" << stats.maxHp << endl;
	cout << "MP: " << stats.mp << "/" << stats.maxMp << endl;
	cout << "POW: " << stats.pow << endl;
	cout << "VIT: " << stats.vit << endl;
	cout << "AGI: " << stats.agi << endl;
	cout << "DEX: " << stats.dex << endl;
	cout << endl;
}

void Unit::clearSkill()
{
	for (size_t i = 0; i < skills.size(); i++)
	{
		delete skills[i];
	}
	skills.clear();
}

void Unit::decreaseMana(int value)
{
	stats.hp -= value;
}

void Unit::getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{

}
