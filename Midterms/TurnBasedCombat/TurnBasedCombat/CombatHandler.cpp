#include "CombatHandler.h"
#include "Enemy.h"
#include "Player.h"
#include "Assassinate.h"
#include "Heal.h"
#include "Shockwave.h"
#include "ObjectStats.h"
#include "NormalAttack.h"

CombatHandler::CombatHandler()
{
	//Normal Attacks
	skills.push_back(new NormalAttack("Throw Knife", "assassin", "Human"));
	skills.push_back(new NormalAttack("Demon's Claw", "assassin", "Demon"));
	skills.push_back(new NormalAttack("Firebolt", "mage", "Human"));
	skills.push_back(new NormalAttack("Shadowball", "mage", "Demon"));
	skills.push_back(new NormalAttack("Holy Blade", "warrior", "Human"));
	skills.push_back(new NormalAttack("Nether Slash", "warrior", "Demon"));
	//Special skills
	skills.push_back(new Assassinate("Coup De Grace", "assassin", "Human", 5));
	skills.push_back(new Assassinate("Hunt", "assassin", "Demon", 5));
	skills.push_back(new Heal("Holy Light", "mage", "Human", 10));
	skills.push_back(new Heal("Necromancy", "mage", "Demon", 10));
	skills.push_back(new Shockwave("Great Lightning", "warrior", "Human", 5));
	skills.push_back(new Shockwave("Berserk", "warrior", "Demon", 5));
	//job, team, name, hp, pow, vit, agi, dex, mp
	units.push_back(new Enemy({ demonWarrior }));
	units.push_back(new Enemy({ demonMage }));
	units.push_back(new Enemy({ demonAssassin }));
	units.push_back(new Player({ humanWarrior }));
	units.push_back(new Player({ humanMage }));
	units.push_back(new Player({ humanAssassin }));

	skillDistributor();
	teamDistributor();
}

deque<Unit*> CombatHandler::bubbleSort(deque<Unit*> unit)
{
	for (size_t i = 0; i < unit.size(); i++)
		for (size_t j = i + 1; j < unit.size(); j++)
		{
			if (unit[i]->getStats().agi < unit[j]->getStats().agi)
				swap(unit[i], unit[j]);
		}
	return unit;
}

void CombatHandler::skillDistributor()
{
	for (size_t i = 0; i < units.size(); i++)
	{
		for (size_t x = 0; x < skills.size(); x++)
		{
			if (skills[x]->getJob() == units[i]->getStats().job && skills[x]->getTeam() == units[i]->getStats().team)
				units[i]->addSkill(skills[x]);
		}
	}
}

void CombatHandler::teamDistributor()
{
	for (size_t i = 0; i < units.size(); i++)
	{
		if (units[i]->getStats().team == "Human")
			playerTeam.push_back(units[i]);
		else
			enemyTeam.push_back(units[i]);
	}
	playerTeamName = playerTeam[0]->getStats().team;
	enemyTeamName = enemyTeam[0]->getStats().team;
}

void CombatHandler::turnOrder()
{
	cout << "===============<Turn Order>===============" << endl;
	for (size_t i = 0; i < units.size(); i++)
	{
		cout << i + 1 << " | " << units[i]->getStats().name << endl;
	}
	cout << "==========================================" << endl;

	cout << "TURN: " << units.front()->getStats().name << endl ;
	cout << "      > HP[" << units.front()->getStats().hp << "] | " << "MP[" << units.front()->getStats().mp << "]" << endl << endl;
}

void CombatHandler::printTeam(deque<Unit*> team, string name)
{
	cout << "===============<" << name << ">================" << endl;
	for (size_t i = 0; i < team.size(); i++)
	{
		cout << "[" << team[i]->getStats().team << "] " << team[i]->getStats().name;
			cout << " HP[" << team[i]->getStats().hp << "/" << team[i]->getStats().maxHp << "] | " 
				<< "MP[" << team[i]->getStats().mp << "/" << team[i]->getStats().maxMp << "]" << endl;
	}

	for (size_t i = 0; i < grave.size(); i++)
	{
		if (grave[i]->getStats().team == name)
			cout << "[" << grave[i]->getStats().team << "] " << grave[i]->getStats().name << " [DEAD]" << endl;
	}

	cout << endl;
}

void CombatHandler::castSkill()
{
	units.front()->getTarget(units.front(), enemyTeam, playerTeam);

	removeDead();

	//Put unit casting spell to back of turn order
	units.push_back(units.front());
	units.pop_front();
}

void CombatHandler::removeDead()
{
	for (size_t i = 0; i < playerTeam.size(); i++)
	{
		if (playerTeam[i]->getStats().hp <= 0)
		{
			playerTeam.erase(playerTeam.begin() + i);
		}
	}
	for (size_t i = 0; i < enemyTeam.size(); i++)
	{
		if (enemyTeam[i]->getStats().hp <= 0)
		{
			enemyTeam.erase(enemyTeam.begin() + i);
		}
	}
	for (size_t i = 0; i < units.size(); i++)
	{
		if (units[i]->getStats().hp <= 0)
		{
			grave.push_back(units[i]);
			units[i]->clearSkill();
			units.erase(units.begin() + i);
		}
	}
}

void CombatHandler::results()
{
	if (enemyTeam.empty() == true)
	{
		cout << "The " << playerTeam[0]->getStats().team << "s WIN!" << endl;
	}
	
	if (playerTeam.empty() == true)
	{
		cout << "The " << enemyTeam[0]->getStats().team << "s WIN!" << endl;
	}
}

void CombatHandler::mainCombat()
{
	units = bubbleSort(units);

	while (enemyTeam.empty() != true && playerTeam.empty() != true)
	{
		printTeam(playerTeam, playerTeamName);
		printTeam(enemyTeam, enemyTeamName);
		turnOrder();
		system("pause");
		system("CLS");

		units.front()->printStats();
		castSkill();
		
		system("pause");
		system("CLS");
	}
	results();
	system("pause");
}
