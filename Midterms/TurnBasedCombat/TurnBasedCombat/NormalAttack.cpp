#include "NormalAttack.h"

NormalAttack::NormalAttack(string name, string job, string team) : Skill(name, job, team)
{

}

void NormalAttack::hitChance(Unit* caster, Unit* target)
{
	float hitRate = ceil((target->getStats().dex / caster->getStats().agi) * 100);

	if (hitRate < 20)
		hitRate = 20;

	if (hitRate > 80)
		hitRate = 80;

	int hitChance = rand() % 100 + 1;

	if (hitRate > hitChance)
		hit = true;
	else
		hit = false;
}

void NormalAttack::printSkill(Unit* caster, Unit* target)
{
	if (hit == false)
	{
		cout << caster->getStats().name << " missed!" << endl;
		return;
	}
	crit = false;

	if (baseDamage == caster->getStats().pow + (caster->getStats().pow * 0.2))
		crit = true;

	cout << caster->getStats().name << " hit " << target->getStats().name
		<< " with " << name << " for " << damage << " damage" << endl;

	if (crit == true)
		cout << "CRITICAL STRIKE!" << endl;
}

deque<Unit*> NormalAttack::autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();

	int index = rand() % enemyTeam.size();
	targets.push_back(enemyTeam[index]);

	return targets;
}

void NormalAttack::setDamage(Unit* caster, Unit* target)
{
	hitChance(caster, target);
	if (hit == true)
		Skill::setDamage(caster, target);
}