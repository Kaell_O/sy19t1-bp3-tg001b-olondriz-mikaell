#pragma once
#include "Skill.h"
class Assassinate : public Skill
{
public:
	Assassinate(string name, string job, string team, int cost);
	void printSkill(Unit* caster, Unit* target);
	deque<Unit*> autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	void setDamage(Unit* caster, Unit* target);
};

