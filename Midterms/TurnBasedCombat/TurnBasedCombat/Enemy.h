#pragma once
#include "Unit.h"
#include <time.h>

class Enemy : public Unit
{
public:
	Enemy(CharacterDef definition);
	void useSkill();
	void chooseSkill();
	void getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
};

