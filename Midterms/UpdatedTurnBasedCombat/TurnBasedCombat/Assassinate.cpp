#include "Assassinate.h"

Assassinate::Assassinate(string name, string job, string team, int pcost) : Skill(name, job, team)
{
	cost = pcost;
}

void Assassinate::printSkill(Unit* caster, Unit* target)
{
	cout << caster->getStats().name << " slaughters " << target->getStats().name << " for " << damage << " damage" << endl;
}

deque<Unit*> Assassinate::autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();
	Unit* target = enemyTeam[0];

	for (size_t i = 0; i < enemyTeam.size(); i++)
	{
		if (target->getStats().hp > enemyTeam[i]->getStats().hp)
			target = enemyTeam[i];
	}

	targets.push_back(target);
	
	return targets;
}

void Assassinate::setDamage(Unit* caster, Unit* target)
{
	baseDamage = ceil(caster->getStats().pow * 2.2);
	damage = caster->getDamage(target, baseDamage);
}