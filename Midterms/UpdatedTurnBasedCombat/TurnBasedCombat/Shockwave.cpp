#include "Shockwave.h"

Shockwave::Shockwave(string name, string job, string team, int pcost) : Skill (name, job, team)
{
	cost = pcost;
}

void Shockwave::printSkill(Unit* caster, Unit* target)
{
	cout << target->getStats().name << " takes " << damage << " damage!" << endl;
}

deque<Unit*> Shockwave::autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();
	for (size_t i = 0; i < enemyTeam.size(); i++)
		targets.push_back(enemyTeam[i]);

	return targets;
}

deque<Unit*> Shockwave::manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets = autoTarget(enemyTeam, playerTeam);

	return targets;
}

void Shockwave::setDamage(Unit* caster, Unit* target)
{
	baseDamage = ceil(caster->getStats().pow * 0.9);
	damage = caster->getDamage(target, baseDamage);
}
