#pragma once
#include <deque>
#include <vector>
#include "Unit.h"
#include "Skill.h"

using namespace std;
class CombatHandler
{
public:
	CombatHandler();
	void mainCombat();
	deque<Unit*> bubbleSort(deque<Unit*> unit);
	void teamDistributor();
	void turnOrder();
	void printTeam(deque<Unit*> team, string name);
	void skillDistributor();
	void castSkill();
	void removeDead();
	void results();
	void clear();

private:
	deque<Unit*> playerTeam;
	deque<Unit*> enemyTeam;
	deque<Unit*> units;
	deque<Unit*> grave;
	vector<Skill*> skills;
	string playerTeamName;
	string enemyTeamName;


};

