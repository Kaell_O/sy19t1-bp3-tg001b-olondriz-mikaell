#include "Heal.h"

Heal::Heal(string name, string job, string team, int pcost) : Skill(name, job, team)
{
	cost = pcost;
}

void Heal::printSkill(Unit* caster, Unit* target)
{
	cout << caster->getStats().name << " heals " << target->getStats().name
	   	<< " for " << healValue << " health!" << endl;
}

deque<Unit*> Heal::autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();
	Unit* target = playerTeam[0];

	for (size_t i = 0; i < playerTeam.size(); i++)
	{
		if (target->getStats().hp > playerTeam[i]->getStats().hp)
			target = playerTeam[i];
	}

	targets.push_back(target);

	return targets;
}

deque<Unit*> Heal::manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	targets.clear();
	int input;
	cout << "> Choose target <" << endl;
	for (size_t i = 0; i < playerTeam.size(); i++)
	{
		cout << "[" << i + 1 << "] " << playerTeam[i]->getStats().name << endl;
	}
	cin >> input;
	targets.push_back(playerTeam[input - 1]);
	return targets;
}

void Heal::setHeal(Unit* caster, Unit* target)
{
	healValue = target->getStats().maxHp * 0.3;
}

void Heal::castSkill(Unit* caster, deque<Unit*> target)
{
	cout << caster->getStats().name << " used " << name << "!" << endl << endl;

	for (size_t i = 0; i < target.size(); i++)
	{
		setHeal(caster, target[i]);
		printSkill(caster, target[i]);
		target[i]->heal(healValue);
	}
}