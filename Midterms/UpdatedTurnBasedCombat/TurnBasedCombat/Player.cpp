#include "Player.h"

Player::Player(CharacterDef definition) : Unit(definition)
{
}

void Player::useSkill()
{
	cout << "Player Test" << endl;
}

void Player::chooseSkill()
{
	int input;
	cout << ">>> Skills" << endl;
	for (size_t i = 0; i < skills.size(); i++)
	{
		cout << "> [" << i + 1 << "]" << " " << skills[i]->getName() << " | Cost: " << skills[i]->getCost() << endl;
	}
	cin >> input;
	while (input > 2 || input < 1)
		cin >> input;

	while (stats.mp < skills[input - 1]->getCost())
	{
		cout << "not enough mana!" << endl;
		cin >> input;
	}

	skill = skills[input - 1];
	stats.mp -= skill->getCost();
}

bool Player::chooseTarget()
{
	int input;

	cout << "[1] Manual Target" << endl;
	cout << "[2] Automatic Target" << endl;

	cin >> input;
	while (input > 2 || input < 1)
		cin >> input;

	if (input == 1)
		return true;
	if (input == 2)
		return false;
}

void Player::getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	chooseSkill();

	if (skill->getName() == "Great Lightning")
	{
		target = skill->autoTarget(enemyTeam, playerTeam);
		skill->castSkill(caster, target);
		return;
	}

	bool chooseTarget = caster->chooseTarget();
	target = skill->autoTarget(enemyTeam, playerTeam);
	if (chooseTarget == true)
		target = skill->manualTarget(enemyTeam, playerTeam);

	skill->castSkill(caster, target);
}
