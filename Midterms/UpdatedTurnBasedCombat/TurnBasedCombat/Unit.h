#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Skill.h"
using namespace std;

class Skill;
struct CharacterDef 
{
	string job;
	string team;
	string name;
	int hp, mp, pow, vit;
	float agi, dex;
	int maxHp, maxMp;
};
class Unit
{
public:
	Unit(CharacterDef definition);
	~Unit();
	virtual void chooseSkill() = 0;
	virtual bool chooseTarget();
	virtual void getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam) = 0;

	CharacterDef getStats();
	int getDamage(Unit* unit, int coefficient);
	Skill* getSkill();
	void applyDamage(int damage);
	void addSkill(Skill* skill);
	void heal(int value);
	void printStats();
	void decreaseMana(int value);

protected:
	vector<Skill*> skills;
	CharacterDef stats;
	deque<Unit*> target;
	Skill* skill;

	string strongAgainst;
};

