#pragma once
#include "Skill.h"
class Shockwave : public Skill
{
public: 
	Shockwave(string name, string job, string team, int cost);
	void printSkill(Unit* caster, Unit* target);
	deque<Unit*> autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	deque<Unit*> manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	void setDamage(Unit* caster, Unit* target);
};

