#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "CombatHandler.h"
#include "NormalAttack.h"
#include <deque>

using namespace std;

int main()
{
	deque<Skill*> skills;
	skills.push_back(new NormalAttack("Throw Knife", "assassin", "Human"));
	skills.push_back(new NormalAttack("Demon's Claw", "assassin", "Demon"));
	skills.push_back(new NormalAttack("Firebolt", "mage", "Human"));
	skills.push_back(new NormalAttack("Shadowball", "mage", "Demon"));
	skills.push_back(new NormalAttack("Holy Blade", "warrior", "Human"));
	skills.push_back(new NormalAttack("Nether Slash", "warrior", "Demon"));

	for (size_t i = 0; i < skills.size(); i++)
	{
		cout << i + 1 << " | " << skills[i]->getName() << endl;
	}
	
	delete skills[3];
	//skills.erase(skills.begin() + 3);
	system("pause");
	for (size_t i = 0; i < skills.size(); i++)
	{
		cout << i + 1 << " | " << skills[i]->getName() << endl;
	}
	system("pause");

	srand(time(NULL));
	CombatHandler* combat = new CombatHandler();
	combat->mainCombat();
}