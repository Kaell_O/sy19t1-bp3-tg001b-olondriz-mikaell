#pragma once
#include "Skill.h"
class NormalAttack : public Skill
{
public:
	NormalAttack(string name, string job, string team);
	void printSkill(Unit* caster, Unit* target);
	deque<Unit*> autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	void hitChance(Unit* caster, Unit* target);
	void setDamage(Unit* caster, Unit* target);

private:
	bool hit;
	bool crit;
};

