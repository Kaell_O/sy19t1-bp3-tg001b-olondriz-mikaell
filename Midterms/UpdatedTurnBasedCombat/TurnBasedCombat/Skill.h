#pragma once
#include <iostream>
#include <string>
#include <deque>
#include <time.h>
#include "Unit.h"

class Unit;
using namespace std;
class Skill
{
public:
	Skill(string name, string job, string team);
	virtual void printSkill(Unit* caster, Unit* target);
	virtual void setDamage(Unit* caster, Unit* target);
	virtual deque<Unit*> autoTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	virtual deque<Unit*> manualTarget(deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
	virtual void castSkill(Unit* caster, deque<Unit*> target);

	string getName();
	string getJob();
	string getTeam();
	int getCost();
	int getDamage();

protected:
	string name;
	int baseDamage;
	int damage;
	string job;
	string team;
	int cost;
	deque<Unit*> targets;

};

