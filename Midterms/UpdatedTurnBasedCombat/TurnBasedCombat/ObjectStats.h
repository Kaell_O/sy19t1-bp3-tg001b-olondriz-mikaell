#pragma once
#include <string>
#include "Unit.h"

CharacterDef demonWarrior = {
  "warrior",  // job
  "Demon",  // team
  "Demon Warrior",  // name
  100,  // hp
  30,  // mp
  40,  // pow
  20,  // vit
  10,  // agi
  30  // dex
};

CharacterDef demonMage = {
  "mage",  // job
  "Demon",  // team
  "Demon Mage",  // name
  80,  // hp
  80,  // mp
  20,  // pow
  25,  // vit
  20,  // agi
  20  // dex
};

CharacterDef demonAssassin = {
  "assassin",  // job
  "Demon",  // team
  "Demon Assassin",  // name
  80,  // hp
  15,  // mp
  30,  // pow
  20,  // vit
  30,  // agi
  45  // dex
};

CharacterDef humanWarrior = {
  "warrior",  // job
  "Human",  // team
  "Human Warrior",  // name
  100,  // hp
  30,  // mp
  40,  // pow
  10,  // vit
  10,  // agi
  30  // dex
};

CharacterDef humanMage = {
  "mage",  // job
  "Human",  // team
  "Human Mage",  // name
  80,  // hp
  80,  // mp
  20,  // pow
  25,  // vit
  20,  // agi
  20  // dex
};

CharacterDef humanAssassin = {
  "assassin",  // job
  "Human",  // team
  "Human Assassin",  // name
  80,  // hp
  15,  // mp
  30,  // pow
  10,  // vit
  30,  // agi
  45  // dex
};