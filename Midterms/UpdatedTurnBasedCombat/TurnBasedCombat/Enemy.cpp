#include "Enemy.h"

Enemy::Enemy(CharacterDef definition) : Unit(definition)
{
}

void Enemy::useSkill()
{
	cout << "Enemy Test" << endl;
}

void Enemy::chooseSkill()
{
	int random = rand() % 2;
	if (skills[random]->getCost() > stats.mp)
		random = 0;
	skill = skills[random];
}

void Enemy::getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam)
{
	chooseSkill();
	target = skill->autoTarget(playerTeam, enemyTeam);

	stats.mp -= skill->getCost();
	skill->castSkill(caster, target);
}
