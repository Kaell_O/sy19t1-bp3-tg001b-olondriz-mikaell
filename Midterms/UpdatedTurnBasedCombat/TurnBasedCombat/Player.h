#pragma once
#include "Unit.h"

class Player : public Unit
{
public:
	Player(CharacterDef definition);
	void useSkill();
	void chooseSkill();
	bool chooseTarget();
	void getTarget(Unit* caster, deque<Unit*> enemyTeam, deque<Unit*> playerTeam);
};

