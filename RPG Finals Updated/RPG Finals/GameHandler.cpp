#include <stack>
#include <time.h>
#include "GameHandler.h"
#include "Player.h"
#include "Menu.h"
#include "Weapon.h"	
#include "Armor.h"

using namespace std;

GameHandler::GameHandler()
{
}

GameHandler::~GameHandler()
{
}

void GameHandler::game(Player* player, Map* map)
{
	system("pause");
	system("CLS");

	srand(time(NULL));

	int playerInput;
	Weapon *weapon = player->getStarterWeapon(player->stats->job);
	Armor *armor = player->getStarterArmor(player->stats->job);

	//Main window
	while (player->stats->hp > 0)
	{
		system("CLS");
		int monsterSpawn = rand() % 100 + 1;

		if (player->exp >= player->maxExp) player->levelUp(player);
		if (player->playerX == 1 && player->playerY == 1)
		{
			Shop *shop = new Shop();
			shop->printShop(shop, player, &weapon, &armor);
			system("CLS");
		}
		map->showMap(player);
		cout << "HP: " << player->stats->hp << "/" << player->stats->maxHp << endl;
		cout << "LEVEL: " << player->level << endl;
		cout << "EXP: " << player->exp << "/" << player->maxExp << endl << endl;
		cout << "What shall we do?" << endl;
		cout << ">>> [1] move | [2] rest | [3] Check Stats <<<" << endl << endl;
		cin >> playerInput;
		if (playerInput == 1)
		{
			player->move(player);
		}
		if (playerInput == 2)
		{
			player->rest(player);
			continue;
		}
		if (playerInput == 3)
		{
			player->printStats(player, weapon, armor);
			system("pause");
			system("CLS");
			continue;
		}
		if (monsterSpawn <= 20)
		{
			continue;
		}
		else
		{
			Monster* monster = map->spawnMonster(monsterSpawn);
			fight(player, monster, weapon, armor);
		}
	}
}

void GameHandler::fight(Player* player, Monster* monster, Weapon* weapon, Armor* armor)
{
	system("CLS");
	int playerDamage;
	int monsterDamage;
	int playerInput;

	cout << ">>> " << monster->stats->name << " APPEARS! <<<" << endl << endl;
	monster->printStats(monster);
	system("pause");
	system("CLS");

	while (true)
	{
		bool run;
		cout << monster->stats->name << "'s HP: " << monster->stats->hp << "/" << monster->stats->maxHp << endl;
		cout << player->stats->name << "'s HP: " << player->stats->hp << "/" << player->stats->maxHp << endl;
		cout << "=========================================================" << endl;
		cout << ">>> [1] FIGHT | [2] RUN AWAY <<<" << endl << endl;
		cin >> playerInput;

		if (playerInput == 1)
		{
			cout << endl;
			//Players turn
			playerDamage = player->attack(player, monster, weapon);
			if (playerDamage == 0) cout << player->stats->name << " MISSES!" << endl;
			else cout << player->stats->name << " hits " << monster->stats->name << " for " << playerDamage << " !" << endl;
			monster->stats->hp -= playerDamage;
			system("pause");

			if (monster->stats->hp <= 0)
			{
				system("CLS");
				cout << ">>> " << monster->stats->name << " SLAIN! <<<" << endl << endl;
				cout << "EXP GAINED: " << monster->killExp << "!" << endl;
				cout << "GOLD AQUIRED: " << monster->killGold << "!" << endl;
				player->gold += monster->killGold;
				player->exp += monster->killExp;
				monster->~Monster();
				system("pause");
				break;
			}
		}
		if (playerInput == 2)
		{
			run = player->run();
			if (run == true)
			{
				cout << player->stats->name << " RAN AWAY!" << endl;
				system("pause");
				break;
			}
			else cout << "YOU FAILED TO RUN AWAY!" << endl; 
		}
		//Monsters turn
		cout << endl;
		monsterDamage = monster->attack(player, monster, armor);
		if (monsterDamage == 0) cout << monster->stats->name << " MISSES!" << endl;
		else cout << monster->stats->name << " hits " << player->stats->name << " for " << monsterDamage << " !" << endl;
		player->stats->hp -= monsterDamage;

		system("pause");
		system("CLS");

		if (player->stats->hp <= 0)
		{
			cout << ">>> YOU DIED <<<" << endl;
			player->~Player();
			break;
		}
	}
}

Player* GameHandler::createCharacter(Player* player)
{
	int chosenClass;
	string playerName;
	cout << "Rise up hero..." << endl;
	cout << "Tell me, what is your name?" << endl << endl;
	cin >> playerName;
	cout << endl;
	cout << "Ah... " << playerName << " what an interesting name." << endl;
	system("pause");
	system("CLS");

	cout << "What is your occupation?" << endl << endl;
	cout << ">>> [1] Warrior <<<" << endl;
	cout << "> 15 HP | 5 Damage | 5 Defence | 5 Agility | 5 Dexterity" << endl;
	cout << "> Starting Weapon: Longsword, 5 Damage" << endl;
	cout << "> Starting Armor: Plate Armor, 2 Armor Rating" << endl << endl;

	cout << ">>> [2] Rogue <<<" << endl;
	cout << "> 13 HP | 10 Damage | 4 Defence | 13 Agility | 8 Dexterity" << endl;
	cout << "> Starting Weapon: Dagger, 2 Damage" << endl;
	cout << "> Starting Armor: Cloth Armor, 1 Armor Rating" << endl << endl;

	cout << ">>> [3] Crusader <<<" << endl;
	cout << "> 20 HP | 2 Damage | 8 Defence | 3 Agility | 4 Dexterity" << endl;
	cout << "> Starting Weapon: Warhammer, 3 Damage" << endl;
	cout << "> Starting Armor: Heavy Plate Armor, 3 Armor Rating" << endl << endl;
	cin >> chosenClass;

	if (chosenClass == 1)
	{
		player = new Player(playerName, "Warrior", 15, 5, 5, 5, 5);
	}
	if (chosenClass == 2)
	{
		player = new Player(playerName, "Rogue", 13, 10, 4, 13, 8);
	}
	if (chosenClass == 3)
	{
		player = new Player(playerName, "Crusader", 20, 2, 5, 3, 4);
	}

	Weapon *weapon = player->getStarterWeapon(player->stats->job);
	Armor *armor = player->getStarterArmor(player->stats->job);

	system("CLS");

	cout << "Very Well, here are the details of your character." << endl << endl;

	player->printStats(player, weapon, armor);

	cout << endl;
	cout << "Goodluck hero, stay safe in your journey." << endl;

	weapon->~Weapon();
	armor->~Armor();
	return player;
}