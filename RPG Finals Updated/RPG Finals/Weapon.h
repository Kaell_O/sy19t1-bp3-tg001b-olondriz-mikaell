#pragma once
#include <string>

using namespace std;

class Weapon
{
public:
	Weapon(string name, int damage, int cost);
	~Weapon();

	string name;
	int stat;
	int price;
};

