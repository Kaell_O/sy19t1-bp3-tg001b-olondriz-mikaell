#pragma once
#include "Player.h"
#include "Monster.h"
#include <string>

class Map
{
public:
	Map();
	~Map();
	void showMap(Player* player);
	Monster* spawnMonster(int spawn);
	void shop();
};

