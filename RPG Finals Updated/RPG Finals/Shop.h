#pragma once
#include <vector>
#include <string>
#include "Weapon.h"
#include "Armor.h"
#include "Player.h"
#include <iostream>

using namespace std;

class Shop
{
public:
	Shop();
	~Shop();

	//stock
	vector<Weapon*> Weapons;
	vector<Armor*> Armors;
	void createWeapons();
	void createArmors();
	void printShop(Shop* shop, Player* player, Weapon** weapon, Armor** armor);
};

