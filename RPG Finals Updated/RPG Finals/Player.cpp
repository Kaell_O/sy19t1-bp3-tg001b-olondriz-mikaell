#include "Player.h"
#include "Map.h"
#include "Monster.h"
#include "Weapon.h"
#include "Armor.h"

Player::Player(string name, string chosenClass, int maxHp, int dmg, int def, int agi, int dex)
{
	stats = new Stats();
	stats->name = name;
	stats->job = chosenClass;
	stats->maxHp = maxHp;
	stats->hp = maxHp;
	stats->dmg = dmg;
	stats->def = def;
	stats->agi = agi;
	stats->dex = dex;

	exp = 0;
	level = 1;
	gold = 0;
	maxExp = 1000;
	playerX = 0;
	playerY = 0;
}

Player::~Player()
{

}

int Player::attack(Player* player, Monster* monster, Weapon* weapon)
{
	int hitRate = (monster->stats->dex / player->stats->agi) * 100;
	if (hitRate < 20) hitRate = 20;
	if (hitRate > 80) hitRate = 80;
	int hitChance = rand() % 100 + 1;
	if (hitRate < hitChance) return 0;

	int damage = (player->stats->dmg + weapon->stat) - monster->armorRate;
	if (damage < 1) damage = 1;
	return damage;
}

void Player::move(Player* player)
{
	int input;
	cout << "> Where shall we head?" << endl;
	cout << "> [1] north, [2] south, [3] east, [4] west" << endl << endl;
	cin >> input;
	if (input == 1)
	{
		player->playerY += 1;
	}
	if (input == 2)
	{
		player->playerY -= 1;
	}
	if (input == 3)
	{
		player->playerX += 1;
	}
	if (input == 4)
	{
		player->playerX -= 1;
	}
	system("CLS");
}

void Player::printStats(Player* player, Weapon* weapon, Armor* armor)
{
	cout << ">>> CHARACTER INFORMATION <<<" << endl << endl; 
	cout << "> Name: " << stats->name << endl;
	cout << "> Class: " << stats->job << endl;
	cout << "> Max HP: " << stats->maxHp << endl;
	cout << "> Damage: " << stats->dmg << endl;
	cout << "> Defence: " << stats->def << endl;
	cout << "> Agility: " << stats->agi << endl;
	cout << "> Dexterity: " << stats->dex << endl << endl;
	cout << "===========================================" << endl << endl;
	cout << ">>> INVENTORY <<<" << endl << endl;
	cout << "> Weapon: " << weapon->name << " | Damage: " << weapon->stat << endl;
	cout << "> Armor: " << armor->name << " | Rating: " << armor->stat << endl;
	cout << "> Gold: " << player->gold << endl;
}

void Player::rest(Player* player)
{
	cout << endl;
	cout << "You sit down and rest" << endl;
	cout << "You feel invigorated, ready for the world" << endl;
	cout << "*hp restored*" << endl << endl;
	player->stats->hp = player->stats->maxHp;
	system("pause");
	system("CLS");
}

void Player::levelUp(Player* player)
{
	int chooseStat = rand() % 5 + 1;
	int statGrowth = rand() % 3 + 1;
	player->level += 1;
	player->maxExp = level * 1000;
	player->exp = 0;

	cout << ">>> LEVEL UP! <<<" << endl << endl;
	cout << "Level: " << player->level - 1 << " -> " << player->level << endl;
	cout << "STAT GAINED: ";
	switch (chooseStat)
	{
		case 1:
			player->stats->maxHp += statGrowth;
			cout << "Max HP + " << statGrowth;
			break;
		case 2:
			player->stats->dmg += statGrowth;
			cout << "Damage + " << statGrowth;
			break;
		case 3:
			player->stats->def += statGrowth;
			cout << "Defence + " << statGrowth;
			break;
		case 4:
			player->stats->agi += statGrowth;
			cout << "Agility + " << statGrowth;
			break;
		case 5:
			player->stats->dex += statGrowth;
			cout << "Dexterity + " << statGrowth;
			break;
	}

	player->stats->hp = player->stats->maxHp;
	cout << endl;
	system("pause");
	system("CLS");
}

bool Player::run()
{
	int random = rand() % 100 + 1;
	int runChance = 25;
	if (runChance >= random) return true;
	else return false;
}

Weapon* Player::getStarterWeapon(string chosenClass)
{
	if (chosenClass == "Warrior") { Weapon *weapon = new Weapon("Long Sword", 5, 0); return weapon; }
	if (chosenClass == "Rogue") { Weapon *weapon = new Weapon("Dagger", 2, 0); return weapon; }
	if (chosenClass == "Crusader") { Weapon *weapon = new Weapon("Warhammer", 3, 0); return weapon; }
}
Armor* Player::getStarterArmor(string chosenClass)
{
	if (chosenClass == "Warrior") { Armor *armor = new Armor("Plate Armor", 3, 0); return armor; }
	if (chosenClass == "Rogue") { Armor *armor = new Armor("Cloth Armor", 1, 0); return armor; }
	if (chosenClass == "Crusader") { Armor *armor = new Armor("Heavy Plate Armor", 2, 0); return armor; }
}