#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Menu.h"
#include "GameHandler.h"
#include "Map.h"

using namespace std;

int main()
{
	GameHandler *handler = new GameHandler();
	Map *map = new Map();
	Player *player = new Player("null", "null", 0, 0, 0, 0, 0);

	player = handler->createCharacter(player);
	handler->game(player, map);

	system("pause");
	return 0;
}