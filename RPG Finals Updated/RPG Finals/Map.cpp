#include "Map.h"
#include "Player.h"
#include "Monster.h"
#include <string>

Map::Map()
{
}


Map::~Map()
{
}

void Map::showMap(Player* player)
{
	cout << "X: " << player->playerX << " | " << "Y: " << player->playerY << endl << endl;
}

Monster* Map::spawnMonster(int monsterSpawn)
{
	//Goblin
	if (monsterSpawn <= 45)
	{
		Monster *goblin = new Monster("Goblin", 10, 4, 1, 3, 10, 9, 100, 10);
		return goblin;
	}
	//Ogre
	else if (monsterSpawn <= 70)
	{
		Monster *ogre = new Monster("Ogre", 14, 8, 3, 5, 6, 7, 250, 50);
		return ogre;
	}
	//Orc
	else if (monsterSpawn <= 95)
	{
		Monster *orc = new Monster("Orc", 16, 10, 5, 5, 5, 7, 500, 100);
		return orc;
	}
	//Orc Lord
	else
	{
		Monster *orcLord = new Monster("Orc Lord", 50, 30, 20, 10, 5, 30, 1000, 1000);
		return orcLord;
	}
}

void Map::shop()
{

}