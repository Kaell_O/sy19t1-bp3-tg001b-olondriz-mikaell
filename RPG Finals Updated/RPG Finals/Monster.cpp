#include "Monster.h"

Monster::Monster(string name, int maxhp, int dmg, int armorRate, int def, int agi, int dex, int exp, int gold)
{
	stats = new Stats();
	stats->name = name;
	stats->hp = maxhp;
	stats->maxHp = maxhp;
	stats->dmg = dmg;
	stats->armorRate = armorRate;
	stats->def = def;
	stats->agi = agi;
	stats->dex = dex;
	killExp = exp;
	killGold = gold;
}

Monster::~Monster()
{
}

int Monster::attack(Player* player, Monster* monster, Armor* armor)
{
	int hitRate = (player->stats->dex / monster->stats->agi) * 100;
	if (hitRate < 20) hitRate = 20;
	if (hitRate > 80) hitRate = 80;
	int hitChance = rand() % 100 + 1;
	if (hitRate < hitChance) return 0;

	int damage = monster->stats->dmg - armor->stat;
	if (damage < 1) damage = 1;
	return damage;
}

void Monster::printStats(Monster* monster)
{
	cout << ">>> MONSTER INFORMATION <<<" << endl << endl;
	cout << "> Name: " << stats->name << endl;
	cout << "> HP: " << stats->maxHp << endl;
	cout << "> Damage: " << stats->dmg << endl;
	cout << "> Armor: " << stats->armorRate << endl;
	cout << "> Defence: " << stats->def << endl;
	cout << "> Agility: " << stats->agi << endl;
	cout << "> Dexterity: " << stats->dex << endl << endl;
}


