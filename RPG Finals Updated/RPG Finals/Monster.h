#pragma once
#include "Player.h"
#include "Armor.h"
#include <string>

using namespace std;
class Player;
class Monster
{
public:
	Monster(string name, int maxhp, int dmg, int armorRate, int def, int agi, int dex, int exp, int gold);
	~Monster();
	void printStats(Monster* monster);
	int attack(Player* player, Monster* monster, Armor* armor);

	int killExp;
	int killGold;
	int armorRate;

	struct Stats* stats;
};
