#pragma once
#include "Player.h"
#include "Menu.h"
#include "Map.h"
#include "Monster.h"
#include "Shop.h"

class GameHandler
{
public:
	GameHandler();
	~GameHandler();

	//Instances
	Player* createCharacter(Player* player);
	void game(Player* player, Map* map);
	void fight(Player* player, Monster* monster, Weapon* weapon, Armor* armor);
};