#include "Shop.h"

Shop::Shop()
{
}

Shop::~Shop()
{
}

void Shop::createWeapons()
{
	Weapon* sword = new Weapon("Short Sword", 5, 10);
	Weapon* longSword = new Weapon("Long Sword", 10, 50);
	Weapon* broadSword = new Weapon("Broad Sword", 20, 200);
	Weapon* excalibur = new Weapon("Excalibur", 999, 9999);

	Weapons.push_back(sword);
	Weapons.push_back(longSword);
	Weapons.push_back(broadSword);
	Weapons.push_back(excalibur);
}

void Shop::createArmors()
{
	Armor* leatherMail = new Armor("Leather Mail", 2, 50);
	Armor* chainMail = new Armor("Chain Mail", 4, 100);
	Armor* darkArmor = new Armor("Dark Armor", 8, 300);

	Armors.push_back(leatherMail);
	Armors.push_back(chainMail);
	Armors.push_back(darkArmor);
}

void Shop::printShop(Shop* shop, Player* player, Weapon** weapon, Armor** armor)
{
	createWeapons();
	createArmors();
	int playerInput;

	while (true)
	{
		system("CLS");
		cout << ">>> WELCOME TO THE SHOP <<<" << endl << endl;
		cout << ">> SWORDS" << endl;
		for (int i = 0; i < shop->Weapons.size(); i++)
		{
			cout << "> " << shop->Weapons[i]->name << " | " << "Damage: " << shop->Weapons[i]->stat << endl;
			cout << "> Price: " << shop->Weapons[i]->price << endl << endl;
		}
		cout << endl;
		cout << ">> ARMOR" << endl;
		for (int i = 0; i < shop->Armors.size(); i++)
		{
			cout << "> " << shop->Armors[i]->name << " | " << "Rate: " << shop->Armors[i]->stat << endl;
			cout << "> Price: " << shop->Armors[i]->gold << endl << endl;
		}

		cout << "GOLD: " << player->gold << endl;
		cout << "What do you want traveller?" << endl;
		cout << ">>> [1] Shop | [9] Leave <<<" << endl;
		cin >> playerInput;
		if (playerInput == 1)
		{
			cout << endl;
			cout << "What do you want?" << endl;
			cout << ">>> [1] Swords | [2] Armor | [3] Back <<<" << endl << endl;
			cin >> playerInput;
			if (playerInput == 1)
			{
				system("CLS");
				cout << "Pick whatever you like, as long as you got the coin!" << endl << endl;
				cout << ">>> SWORDS <<<" << endl;
				for (int i = 0; i < shop->Weapons.size(); i++)
				{
					cout << "> [" << i << "]" << shop->Weapons[i]->name << " | " << "Damage: " << shop->Weapons[i]->stat << endl;
					cout << "> Price: " << shop->Weapons[i]->price << endl << endl;
				}
				cout << "GOLD: " << player->gold << endl;
				cout << ">>> [9] BACK <<<" << endl << endl;

				cin >> playerInput;
				if (playerInput == 9) continue;
				else if (playerInput > Weapons.size()) cout << "I don't have an item there?!" << endl;
				else if (player->gold > Weapons[playerInput]->price)
				{
					player->gold -= Weapons[playerInput]->price;
					cout << ">>> PURCHASED " << Weapons[playerInput]->name << "! <<<" << endl;
					(*weapon)->~Weapon();
					*weapon = Weapons[playerInput];
					shop->Weapons.erase(Weapons.begin() + playerInput);
					system("pause");
					continue;
				}
				else cout << "You don't have enough gold!" << endl; system("pause"); continue;
				continue;
			}

			if (playerInput == 2)
			{
				system("CLS");
				cout << "Pick whatever you like, as long as you got the coin!" << endl << endl;
				cout << ">>> ARMOR <<<" << endl;
				for (int i = 0; i < shop->Armors.size(); i++)
				{
					cout << "> [" << i << "]" << shop->Armors[i]->name << " | " << "Rate: " << shop->Armors[i]->stat << endl;
					cout << "> Price: " << shop->Armors[i]->gold << endl << endl;
				}
				cout << "GOLD: " << player->gold << endl;
				cout << ">>> [9] BACK <<<" << endl << endl;
				cin >> playerInput;
				if (playerInput == 9) continue;
				else if (playerInput > Armors.size()) cout << "I don't have an item there?!" << endl;
				else if (player->gold > Armors[playerInput]->gold)
				{
					player->gold -= Armors[playerInput]->gold;
					cout << ">>> PURCHASED " << Armors[playerInput]->name << "! <<<" << endl;
					(*armor)->~Armor();
					*armor = Armors[playerInput];
					shop->Armors.erase(Armors.begin() + playerInput);
					system("pause");
					continue;
				}
				else cout << "You don't have enough gold!" << endl; system("pause"); continue;
				continue;
			}
		}
		if (playerInput == 9)
		{
			cout << "Farewell traveller!" << endl << endl;
			system("pause");
			break;
		}
	}
	shop->~Shop();
}