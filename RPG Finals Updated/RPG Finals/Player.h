#pragma once
#include <iostream>
#include <string>
#include "Weapon.h"
#include "Armor.h"
#include "Monster.h"

using namespace std;
class Monster;
class Player
{
public:
	//Create Player
	Player(string name, string chosenClass, int maxHp, int dmg, int def, int agi, int dex);
	~Player();

	//Actions
	void move(Player* player);
	void printStats(Player* player, Weapon* weapon, Armor* armor);
	int attack(Player* player, Monster* monster, Weapon* weapon);
	void rest(Player* player);
	void levelUp(Player* player);
	bool run();

	Weapon* getStarterWeapon(string chosenClass);
	Armor* getStarterArmor(string chosenClass);
	
	int playerX;
	int playerY;

	struct Stats* stats;
	int exp;
	int maxExp;
	int level;
	int gold;
	string armor;
	string weapon;

};

struct Stats
{
	string name;
	string job;
	int hp;
	int maxHp;
	int dmg; //add 1 damage to base
	int def; //reduces 1 damage
	int agi; //evasion rate
	int dex; //hit rate
	int armorRate;
};