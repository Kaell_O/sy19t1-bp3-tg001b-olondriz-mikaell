#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet::~Planet()
{
	delete mNode;
}

Planet* Planet::createPlanet(SceneManager &sceneManager, float size, ColourValue colour, std::string matName, Vector3 location)
{
	ManualObject* manual = sceneManager.createManualObject();

	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(matName, "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setAmbient(colour);

	if (matName == "sunMat")
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(1, 1, 0);

	manual->begin(matName, RenderOperation::OT_TRIANGLE_LIST);

	std::vector<Vector3> vertices;

	//Compute for vertex positions
	int stacks = 16;
	int slices = 16;
	float radius = size / 2;

	Vector3 top(0, radius, 0);
	vertices.push_back(top);

	//Formula from https://stackoverflow.com/questions/4081898/procedurally-generate-a-sphere-mesh#comment4389166_4081898
	for (int i = 1; i < stacks; i++)
	{
		for (int j = 0; j < slices; j++)
		{
			float x = radius * Math::Sin(Math::PI * i / stacks) * Math::Cos(2 * Math::PI * j / slices);
			float y = radius * Math::Cos(Math::PI * i / stacks);
			float z = radius * Math::Sin(Math::PI * i / stacks) * Math::Sin(2 * Math::PI * j / slices);

			Vector3 vert(x, y, z);
			vertices.push_back(vert);
		}
	}

	Vector3 bot(0, -radius, 0);
	vertices.push_back(bot);

	//Set normals and positions
	manual->position(top);
	Vector3 topNorm = vertices[0].normalisedCopy();
	manual->normal(topNorm);

	for (int i = 1; i <= (stacks - 1) * slices; i++)
	{
		manual->colour(colour);
		manual->position(vertices[i]);
		Vector3 normal = vertices[i].normalisedCopy();
		manual->normal(normal);
	}

	manual->position(bot);
	Vector3 botNorm = vertices[vertices.size() - 1].normalisedCopy();
	manual->normal(botNorm);

	//Indexing
	for (int i = 0; i < stacks; i++)
	{
		int vertexOnTop = slices * (i-1);
		int vertexOnLevel = slices * i;
		//Create top
		if (i == 0)
		{
			for (int j = 1; j < slices; j++)
			{
				manual->index(j);
				manual->index(0);
				manual->index(j + 1);
			}
			// Loop back for last triangle
			manual->index(slices);
			manual->index(0);
			manual->index(1);
		}

		//Create bottom, different block of code due to different render order
		else if (i == stacks - 1)
		{
			int tip = vertices.size() - 1;
			for (int j = 1; j < slices; j++)
			{
				manual->index(tip);
				manual->index(j + vertexOnTop);
				manual->index(j + (vertexOnTop + 1));
			}
			// Loop back for last triangle
			manual->index(tip);
			manual->index(vertexOnLevel);
			manual->index(1 + vertexOnTop);
		}

		//Create body, creates boxes for the main body of the sphere
		else
		{
			for (int j = 1; j < slices; j++)
			{
				manual->index(j + (vertexOnTop + 1));
				manual->index(j + (vertexOnLevel + 1));
				manual->index(j + vertexOnLevel);
				manual->index(j + (vertexOnTop + 1));
				manual->index(j + vertexOnLevel);
				manual->index(j + vertexOnTop);
			}

			//Create last box
			manual->index(1 + vertexOnTop);
			manual->index(1 + vertexOnLevel);
			manual->index((slices - 1) + (vertexOnLevel + 1));
			manual->index(1 + vertexOnTop);
			manual->index((slices - 1) + (vertexOnLevel + 1));
			manual->index((slices - 1) + (vertexOnTop + 1));
		}
	}

	manual->end();

	SceneNode* newNode;
	newNode = sceneManager.getRootSceneNode()->createChildSceneNode(location);
	newNode->attachObject(manual);
	Planet* planet = new Planet(newNode);

	return planet;
}

SceneNode& Planet::getNode()
{
	return *mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
	float pX = mParent->getNode().getPosition().x;
	float pZ = mParent->getNode().getPosition().z;

	mNode->setPosition(mNode->getPosition().x + pX, 0, mNode->getPosition().z + pZ);
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}

void Planet::update(const FrameEvent& evt)
{
	Degree rotation = Degree(mLocalRotationSpeed);
	mNode->rotate(Vector3(0, 1, 0), Radian(rotation) * evt.timeSinceLastFrame);

	if (mParent == NULL)
		return;

	float pX = mParent->getNode().getPosition().x;
	float pZ = mParent->getNode().getPosition().z;

	const float oldX = mNode->getPosition().x - pX;
	const float oldZ = mNode->getPosition().z - pZ;
	const float oldY = mNode->getPosition().y;

	float newX = oldX * cos(mRevolutionSpeed * evt.timeSinceLastFrame) + oldZ * sin(mRevolutionSpeed * evt.timeSinceLastFrame);
	float newZ = oldX * -sin(mRevolutionSpeed * evt.timeSinceLastFrame) + oldZ * cos(mRevolutionSpeed * evt.timeSinceLastFrame);

	mNode->setPosition(newX + pX, oldY, newZ + pZ);
}
