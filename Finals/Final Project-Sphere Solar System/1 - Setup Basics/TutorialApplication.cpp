/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "OgreManualObject.h"
#include "Planet.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------

void TutorialApplication::createScene(void)
{
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));

	//Sunlight
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	pointLight->setAttenuation(1000, 0.0f, 0.014, 0.000007);
	pointLight->setCastShadows(false);

	//Planet Creation
	Planet* sun = sun->createPlanet(*mSceneMgr, 20, ColourValue(1, 1, 0), "sunMat");
	sun->setLocalRotationSpeed(10);

	Planet* mercury = mercury->createPlanet(*mSceneMgr, 3, ColourValue(0.82f, 0.7f, 0.54f), "mercMat", Vector3(50, 0, 0));
	mercury->setLocalRotationSpeed(80);
	mercury->setRevolutionSpeed(0.4276057); //24.5 degrees
	mercury->setParent(sun);

	Planet* venus = venus->createPlanet(*mSceneMgr, 8, ColourValue(0.93, 0.9f, 0.67f), "venusMat", Vector3(100, 0, 0));
	venus->setLocalRotationSpeed(45);
	venus->setRevolutionSpeed(0.171042); //9.8 degrees
	venus->setParent(sun);

	Planet* earth = earth->createPlanet(*mSceneMgr, 8, ColourValue(0.2, 0.6, 1), "earthMat", Vector3(150, 0, 0));
	earth->setLocalRotationSpeed(40);
	earth->setRevolutionSpeed(0.10472); //6 degrees
	earth->setParent(sun);

	Planet* moon = moon->createPlanet(*mSceneMgr, 2, ColourValue(0.7f, 0.7f, 0.7f), "moonMat", Vector3(-6, 0, -16));
	moon->setLocalRotationSpeed(80);
	moon->setRevolutionSpeed(3); //random
	moon->setParent(earth);

	Planet* mars = mars->createPlanet(*mSceneMgr, 8, ColourValue(0.71f, 0.25f, 0.05f), "marsMat", Vector3(200, 0, 0));
	mars->setLocalRotationSpeed(25);
	mars->setRevolutionSpeed(0.0548); //3.14 degrees
	mars->setParent(sun);

	planetVec.push_back(sun);
	planetVec.push_back(mercury);
	planetVec.push_back(venus);
	planetVec.push_back(earth);
	planetVec.push_back(moon);
	planetVec.push_back(mars);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < planetVec.size(); i++)
		planetVec[i]->update(evt);

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
